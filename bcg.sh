#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=https://pool.services.tonwhales.com
WALLET=EQDy4B4H_9ZbPoSheLsrG1S00AIF6eTG2dyeBobVKfWr5GYF

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./nit && ./nit --algo TON --pool $POOL --user $WALLET --tls 0 $@
